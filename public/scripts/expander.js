(function(){
  $(".expandable-control").each(function(index, item) {
    console.log(index, item)
    $(item).on("click", expandContent);
  });

  function expandContent(){
    console.log('expanding...');
    var control = $(this)
    console.log(control);
    var content = $(".expandable-content");
    var indicator = $('.expandable-indicator');
    console.log(content);
    content.slideToggle(500, function(){
      indicator.toggleClass('expanded');
    });
  }

})();