var reviews = {
  "data": [
    {
      "created_time": "2017-05-14T16:05:42+0000",
      "reviewer": {
        "name": "Amy Opperer Brode",
        "id": "10204438543182141"
      },
      "rating": 5
    },
    {
      "created_time": "2017-05-11T02:55:48+0000",
      "reviewer": {
        "name": "Genevieve Maiani",
        "id": "10152357033893270"
      },
      "rating": 5
    },
    {
      "created_time": "2017-05-11T00:19:08+0000",
      "reviewer": {
        "name": "Melanie Parker",
        "id": "10153319422910516"
      },
      "rating": 5
    },
    {
      "created_time": "2017-04-23T19:17:20+0000",
      "reviewer": {
        "name": "Jared Sparr",
        "id": "830030020409858"
      },
      "rating": 5
    },
    {
      "created_time": "2017-04-07T22:46:09+0000",
      "reviewer": {
        "name": "Cheryl Phillips",
        "id": "10203079808703807"
      },
      "rating": 5,
      "review_text": "Did a private lesson for kids to try it. They loved it. Shawn was terrific  with them.  Wish they had a place closer to Novi"
    },
    {
      "created_time": "2017-03-23T01:54:27+0000",
      "reviewer": {
        "name": "Resa Pawlan",
        "id": "10152878691544485"
      },
      "rating": 5,
      "review_text": "Love love love.... Everyone there  is so welcoming and kind, accommodating to your personal abilities and needs. They make a terribly uncoordinated person like myself feel graceful, strong and beautiful! Aerial acrobatics has been the single most challenging physical activity I've ever done, and I worked so hard to achieve small victories in the classes I took. They were able to work with my limitations, and still push me to accomplish more than I ever thought. Thank you Micha and everyone else for believing in me! I wish I still lived in Detroit so I could take more classes. Xoxo"
    },
    {
      "created_time": "2017-03-16T03:07:13+0000",
      "reviewer": {
        "name": "Willow Blossom Farms",
        "id": "886188464786090"
      },
      "rating": 5,
      "review_text": "What an amazing place! As a parent I am impressed with how much my daughter has learned in the short amount of time she has attended classes here. She absolutely loves going to class and it is worth the 3 hour roundtrip drive twice a week for us."
    },
    {
      "created_time": "2017-03-13T00:17:26+0000",
      "reviewer": {
        "name": "Sarah E. Medley Pettigrew",
        "id": "10205447777738881"
      },
      "rating": 5,
      "review_text": "Amazing birthday party experience!!! All of the children were engaged the entire time.  Safe, fun, incredible!!!"
    },
    {
      "created_time": "2017-02-28T04:08:32+0000",
      "reviewer": {
        "name": "Nakia Gaither",
        "id": "10203658571014412"
      },
      "rating": 5
    },
    {
      "created_time": "2017-02-23T02:20:24+0000",
      "reviewer": {
        "name": "Kasey Pierce",
        "id": "10201735891894141"
      },
      "rating": 5
    },
    {
      "created_time": "2017-02-21T00:04:26+0000",
      "reviewer": {
        "name": "Kalyani Fae Siddhi",
        "id": "10152401711597922"
      },
      "rating": 5
    },
    {
      "created_time": "2017-02-05T11:51:25+0000",
      "reviewer": {
        "name": "Kathleen Sparr",
        "id": "10204380413526902"
      },
      "rating": 5,
      "review_text": "Wonderful teachers that love what they do and it shows through the instruction and lessons; they keep it safe, challenging and so much fun.   I never did anything like this until about a year ago and now the things I can do excite me and amaze me.   Thanks so much to Anthony and Veronica who teach the Acro 1 & 2 class and to Matt that teaches the Arial 1 that I'm able to do when I miss my Acro class."
    },
    {
      "created_time": "2017-01-29T18:57:44+0000",
      "reviewer": {
        "name": "Erikka Johnson",
        "id": "1642085019404192"
      },
      "rating": 5
    },
    {
      "created_time": "2017-01-23T02:08:21+0000",
      "reviewer": {
        "name": "Melissa Ibarra",
        "id": "10154011798815697"
      },
      "rating": 5,
      "review_text": "I am taking private lessons with my friend and we've had some awesome teachers!!! Highly recommend any classes they teach because everyone is super knowledgeable and very professional!!"
    },
    {
      "created_time": "2017-01-22T00:52:33+0000",
      "reviewer": {
        "name": "Chelsea Chandler",
        "id": "10152883855038373"
      },
      "rating": 5
    },
    {
      "created_time": "2017-01-09T16:52:17+0000",
      "reviewer": {
        "name": "Amy Vaughn Mulrenin",
        "id": "10153940050996051"
      },
      "rating": 5,
      "review_text": "The staff is extremely supportive! I was a bit nervous due to the lack of upper body strength...but with the encouragement from our instructors, I was able to do everything included in my session �. Amazing experience!"
    },
    {
      "created_time": "2017-01-09T00:04:05+0000",
      "reviewer": {
        "name": "Heather Martin",
        "id": "746426465444270"
      },
      "rating": 5,
      "review_text": "Very supportive staff- their grace makes it looks easy- they were very helpful in their directions and options. I really liked having an opportunity to try each of the styles they offer and even watching a young girl in her lesson in complete amazement of her skills."
    },
    {
      "created_time": "2017-01-08T23:48:59+0000",
      "reviewer": {
        "name": "Cierra Marie",
        "id": "10206464784839591"
      },
      "rating": 5,
      "review_text": "I booked a private party here for my birthday and had an amazing time. Mark and Cortney were the instructors and they were both so nice and fun! I had a great experience"
    },
    {
      "created_time": "2016-12-30T15:33:37+0000",
      "reviewer": {
        "name": "Laurel Miller",
        "id": "10152480956181532"
      },
      "rating": 5
    },
    {
      "created_time": "2016-12-29T20:09:28+0000",
      "reviewer": {
        "name": "Sara Stevenson",
        "id": "852598194756803"
      },
      "rating": 5,
      "review_text": "I had my first lesson here on Christmas morning and it was fun and comfortable. I wasn't sure what to expect but they made sure that we were walked through everything and I felt very encouraged. I can't wait for the next class!"
    },
    {
      "created_time": "2016-12-29T20:00:28+0000",
      "reviewer": {
        "name": "Mark Gonzales",
        "id": "10208066020903337"
      },
      "rating": 5,
      "review_text": "I mean, wow. I just became a teacher at the Flyhouse 3 months ago. Before that, I was a student and have always been impressed with the high energy and fun atmosphere at the Flyhouse. The teachers and students are all amazing. The support I've received from the veteran teachers and Micha has made me feel at home and competent. Teaching has been a pleasure. Seems like everyone who comes into the Flyhouse leaves with a smile on their face (helps that we're always reminding people to smile and point their toes). The best part for me is the opportunity to fly and express myself in beauty and strength. Then also to make that possible for others. Thank you so much for these realities :-)"
    },
    {
      "created_time": "2016-12-28T22:37:42+0000",
      "reviewer": {
        "name": "Valerie Zelin",
        "id": "10208373247703097"
      },
      "rating": 5,
      "review_text": "I have been taking classes at the Detroit Flyhouse for 2.5 years. This is a great place to not only improve your strength/flexibility but to improve self-confidence and challenge yourself to do things you never thought you could do. I have never been a \"performer,\" but taking lyra choreography classes has given me the confidence to perform in 2 student/teacher showcases, and now I absolutely love it! The Flyhouse staff is always encouraging, positive, and helpful!"
    },
    {
      "created_time": "2016-12-27T22:26:13+0000",
      "reviewer": {
        "name": "William Haranczak",
        "id": "1022944127721821"
      },
      "rating": 5
    },
    {
      "created_time": "2016-12-27T21:56:43+0000",
      "reviewer": {
        "name": "Ken McGreevy",
        "id": "107723286253710"
      },
      "rating": 5
    },
    {
      "created_time": "2016-12-27T19:38:57+0000",
      "reviewer": {
        "name": "Christie Smith",
        "id": "10155504204375301"
      },
      "rating": 5
    }
  ],
  "paging": {
    "cursors": {
      "before": "MTE4ODI5MDc2OToxNjAxNTgxNzQwNTc3ODIZD",
      "after": "NTE2MDYwMzAwOjE2MDE1ODE3NDA1Nzc4MgZDZD"
    },
    "next": "https://graph.facebook.com/v2.9/160158174057782/ratings?access_token=<access token sanitized>&pretty=0&limit=25&after=NTE2MDYwMzAwOjE2MDE1ODE3NDA1Nzc4MgZDZD"
  }
};

var withText = _.pickBy(reviews.data, function(value, key){
  return value && value.review_text && typeof value.review_text === 'string' && value.review_text.length > 0;
});

const fragment = document.createDocumentFragment();

var htmlRatings = _.transform(withText, function(accumulator, value, key, object){
  var review = document.createElement('div');
  var text = document.createElement('p');
  var ratingContainer = document.createElement('div');
  var ratingTop = document.createElement('div');
  var ratingBottom = document.createElement('div');

  review.classList = 'review-text';

  ratingContainer.classList = 'star-ratings-css';
  ratingTop.classList = 'star-ratings-css-top';
  ratingBottom.classList = 'star-ratings-css-bottom'

  let top = _.times(value.rating, function () {
    var star = document.createElement('span');
    star.textContent = '★';
    return star;
  }).forEach(function(rating){
    ratingTop.appendChild(rating);
  });


  _.times(5, function() {
    var star = document.createElement('span');
    star.textContent = '★';
    return star;
  }).forEach(function(rating){
    ratingBottom.appendChild(rating);
  });

  ratingContainer.appendChild(ratingTop);
  ratingContainer.appendChild(ratingBottom);

  text.textContent = value.review_text;
  text.classList = 'review-text'

  review.appendChild(ratingContainer);
  review.appendChild(text);

  accumulator.push(review);
}, []);

htmlRatings.forEach(function(rating){
  fragment.appendChild(rating);
});

document.getElementById('reviewsContainer').appendChild(fragment);