function init(){
  var reviewsEl = document.getElementById('reviews');
  var reviews   = reviewsEl.children[1];
  var leftBtn   = reviewsEl.children[0];
  var rightBtn  = reviewsEl.children[2];


  var index = 0;
  var VISIBLE = 2;
  var max = [].slice.apply(reviews.children).length;

  leftBtn.onclick  = goLeft;
  window.goLeft    = goLeft;

  rightBtn.onclick = goRight;
  window.goRight   = goRight;


  function goLeft(){
    console.log('left');
    if (index === 0) {
      return;
    }

    index--;
    scroll(index)
  }

  function scroll(index){
    if(!reviews) {
      reviews = reviewsEl.children[1];
    }

    reviews.scrollLeft = index * 350
    // reviews.scrollTo
  }

  function goRight(){
    if (index + VISIBLE >= max) {
      return;
    }

    index++;
    scroll(index);
  }
}