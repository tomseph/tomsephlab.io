(function() {
  var NUM_ITEMS = document.getElementsByClassName("carousel-item").length;
  var ITEM_LIMIT = NUM_ITEMS - 1;
  var DEGREE = 360 / NUM_ITEMS;

  var currdeg = 0;
  var num = 0;

  var carousel = {
    0: { prev: 5,   next: 1},
    1: { prev: 0,   next: 2},
    2: { prev: 1,   next: 3},
    3: { prev: 2,   next: 4},
    4: { prev: 3,   next: 5},
    5: { prev: 4,   next: 0}
  };

  setClickHandlers(0);

  function setClickHandlers(center) {
    var current = $(".item" + center);
    var prev = $(".item" + carousel[center].prev);
    var next = $(".item" + carousel[center].next);

    prev.on("click", { direction: "left" }, rotate);
    current.on("click", goPage.bind(null, 0));
    next.on("click", { direction: "right" }, rotate);
  }

  function removeHandlers(center) {
    var current = $(".item" + center);
    var prev = $(".item" + carousel[center].prev);
    var next = $(".item" + carousel[center].next);

    current.off('click');
    prev.off('click');
    next.off('click');
  }

  function rotate(event) {
    removeHandlers(num);
    $(".text" + num).fadeOut();

    if (event.data.direction === "right") {
      currdeg = currdeg - DEGREE;
      num = num === ITEM_LIMIT ? 0 : num + 1;
    }

    if (event.data.direction == "left") {
      currdeg = currdeg + DEGREE;
      num = num === 0 ? ITEM_LIMIT : num - 1;
    }


    $(".text" + num).fadeIn();

    $(".carousel-item").each(fadeInOut);

    setTimeout(function() {
      setClickHandlers(num);
    }, 1000);

    $(".carousel").css({
      "-webkit-transform": "rotateY(" + currdeg + "deg)",
      "-moz-transform": "rotateY(" + currdeg + "deg)",
      "-o-transform": "rotateY(" + currdeg + "deg)",
      transform: "rotateY(" + currdeg + "deg)"
    });

    $(".carousel-item").css({
      "-webkit-transform": "rotateY(" + -currdeg + "deg)",
      "-moz-transform": "rotateY(" + -currdeg + "deg)",
      "-o-transform": "rotateY(" + -currdeg + "deg)",
      transform: "rotateY(" + -currdeg + "deg)"
    });
  }

  function goPage(page) {
    page = page || 0;
    var pages = [
      "class-private.html",
      "classes.html",
      "class-group.html",
      "workshops.html",
      "classes.html",
      "classes.html"
    ];
    location.href = location.origin + "/" + pages[page];
  }

  function fadeInOut(index, item) {
    var curr = num;
    var next = carousel[curr].next;
    var prev = carousel[curr].prev;

    if (index === num || index === prev || index === next) {
      $(item).fadeIn();
    } else {
      $(item).fadeOut();
    }
  }
})();
